// jshint browser:true, eqeqeq:true, undef:true, devel:true, esversion: 6

const bouton = document.querySelector("article button");
const color = document.querySelector("#color");

// eventListener pour changer la taille des images qui s'affichent
bouton.addEventListener("click", () => {
    
    const height = document.querySelector("#h").value;
    const width = document.querySelector("#w").value;
    const regex = /[0-9]+/;

    if(height.match(regex) && width.match(regex)) {        // en gros, si c'est un nombre
        const posterImgs = document.querySelectorAll("#poster img");
        for(let i = 0 ; i < posterImgs.length ; i++) {
            let img = posterImgs[i];
            img.style.height = height + "px";
            img.style.width = width + "px";
        }
    } else alert("Entrez des vrais paramètres espèce de bouffon");
});

// eventListener pour s'occuper de la couleur de fond du poster
color.addEventListener('change', () => {
    const poster = document.querySelector('#poster');
    poster.style.backgroundColor = color.value;
});

// La gestion du choix du thème se fait dans le scriptProjet.
// Il en est de même pour la gestion du drag and drop.










