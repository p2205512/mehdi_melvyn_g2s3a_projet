// jshint browser:true, eqeqeq:true, undef:true, devel:true, esversion: 6

// éléments pour pouvoir initialiser les requêtes à l'API (nous utiliserons ici Pexels)
const url = "https://api.pexels.com/v1/search?query=";
const key = "eVDIHGZjgatqzAJbPuV0Lc522ZhZUJk7jyEkkZ6639jsuQC47TwIc7vz";
const options = {
	method: "GET",
	headers: {
		Authorization: key
	}
};

// partie bonus pour le thème : tout a été mis dans un EventListener
document.querySelector("#boutonTheme").addEventListener('click', () => {

	// on regarde si le thème est utilisable
	let theme = document.querySelector("#theme").value;
	if(theme === null || theme === undefined) {
		theme = "tiger";
	}

	// on réinitialise les zones
	document.getElementById("poster").innerHTML = '';

	let lesImages = document.querySelectorAll('aside img');
	if(lesImages.length === 6) {
		for(let i = 0 ; i < lesImages.length ; i++) {
			document.querySelector('aside').removeChild(lesImages[i]);
		}
	}


	/*
	ZONE DE DEFINITION DES 4 ZONES DU POSTER PRINCIPAL
	*/

	const poster = document.getElementById("poster");

	// on ajoute les 4 cadres vides au poster
	for (let i = 1; i <= 4 ; i++) {
		const emplacementImage = document.createElement("img");
		emplacementImage.setAttribute("alt", "zone "+i);
		emplacementImage.className = "depot";
		poster.appendChild(emplacementImage);

		emplacementImage.addEventListener('dragover', (evt) => {
			evt.preventDefault(); 	// par défaut, l'image reviendra à sa place. On évite ceci.
		});

		emplacementImage.addEventListener('drop', (evt) => {
			// on change l'attribut SRC de la zone en question avec le SRC que l'on avait stocké
			emplacementImage.setAttribute('src', evt.dataTransfer.getData('src'));
		});
	}

	// l'EventListener fait en sorte qu'au clic sur une des cases, elle "ait l'air" sélectionnée (cadre rouge)
	poster.addEventListener('click', (evt) => {
		let cible = evt.target;
		
		const emplacementsImage = document.getElementsByTagName("img");
		for (const emplacementImage of emplacementsImage) {
			if (emplacementImage.getAttribute("alt") !== null && emplacementImage.getAttribute("alt") === cible.getAttribute("alt") ) {
				emplacementImage.className = "depot selected";
			} else {
				emplacementImage.className = "depot";
			}
		}
	});


	/*
	ZONE DE DEFINITION DES 6 IMAGES DU FETCH
	*/

	// on boucle 6 fois pour récupérer 6 photos
	for (let i = 1; i <= 6; i++) {
		
		// on crée une nouvelle image...
		const depot = document.createElement("img");

		// ...à laquelle on ajoute un EventListener qui lui permet de savoir quand elle est cliquée pour la placer
		depot.addEventListener('click', (evt) => {
			const cible = evt.target;
			
			// on récupère tous les cadres image de la zone du milieu
			const emplacementsImage = document.getElementsByTagName("img");
			for (const emplacementImage of emplacementsImage) {
				// si on est tombé sur le cadre sélectionné, on lui ajoute un lien vers la photo pour qu'il l'affiche
				if (emplacementImage.className === "depot selected") {
					emplacementImage.setAttribute("src", cible.src);
					// on lui retire la classe Selected
					emplacementImage.className = "depot";
				}
			}
		});

		// EventListener pour le drag and drop
		depot.addEventListener('dragstart', (evt) => {
			evt.dataTransfer.setData('src', evt.target.src);
			// on peut se servir de cette donnée pour passer des éléments d'un event à l'autre
			// on lui donne donc le lien vers l'image à déplacer  
		});
		
		// partie requête API
		fetch(url + theme, options)
			.then(function(response) {return response.json();})
			.then(function(data) {
				let images = data.photos;
				
				depot.setAttribute("src", ""+images[i].src.tiny);
				depot.setAttribute("draggable", "true");
				depot.className = "depot";
				
				// on ajoute la photo à la zone de gauche
				const aSide = document.querySelector("aside");
				aSide.appendChild(depot);
			})
			.catch(function(error) {
				console.log(error);

				// lien vers les images prédéfinies dans notre dossier img
				let image = "img/img" + i + ".jpg";
				
				depot.setAttribute("src", image);
				depot.className = "depot";
				
				// on ajoute la photo à la zone de gauche
				const aSide = document.querySelector("aside");
				aSide.appendChild(depot);

			});
	}

});
